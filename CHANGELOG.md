# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.1.0 (2020-05-28)


### Features

* first working version ([be45dc8](https://gitlab.com/guardianproject-ops/terraform-aws-ses-cloudflare/commit/be45dc8cdb8c9d1196c15e4a030f567e897c3d96))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
